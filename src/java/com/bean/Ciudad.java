/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import com.bean.lazy.LazyCiudad;
import com.model.controller.ControllerCiudad;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author desertbarret
 */
@ManagedBean
@RequestScoped
public class Ciudad {

    private LazyCiudad LazyCiudad;

    public Ciudad() {
        this.LazyCiudad = new LazyCiudad(new ControllerCiudad().list());
    }

    public LazyCiudad getLazyCiudad() {
        return LazyCiudad;
    }

    public void setLazyCiudad(LazyCiudad LazyCiudad) {
        this.LazyCiudad = LazyCiudad;
    }

}
