
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean.lazy;

/**
 *
 * @author desertbarret
 */
import com.model.controller.ControllerCiudad;
import com.model.entitie.EntitieCiudad;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * Dummy implementation of LazyDataModel that uses a list to mimic a real
 * datasource like a database.
 */
public class LazyCiudad extends LazyDataModel<EntitieCiudad> {

    private List<EntitieCiudad> datasource;

    public LazyCiudad(List<EntitieCiudad> datasource) {
        this.datasource = datasource;
    }

    @Override
    public EntitieCiudad getRowData(String rowKey) {
        for (EntitieCiudad EntitieCiudad : datasource) {
            if (EntitieCiudad.getId().equals(rowKey)) {
                return EntitieCiudad;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(EntitieCiudad EntitieCiudad) {
        return EntitieCiudad.getId();
    }

    @Override
    public int getRowCount() {
        return new ControllerCiudad().count();
    }

    @Override
    public int getPageSize() {
        return new ControllerCiudad().count();
    }

    @Override
    public List<EntitieCiudad> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        datasource = new ControllerCiudad().list(first, pageSize);
        return datasource;
    }
}
