/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model.controller;

import com.model.Conexion;
import com.model.entitie.EntitieCiudad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author desertbarret
 */
public class ControllerCiudad {

    public int count() {
        try (Connection conectar = new Conexion().conectar()) {
            try (PreparedStatement pstmt = conectar.prepareStatement("SELECT COUNT(*) AS total FROM karviewcnt.ciudad LIMIT 1;"); ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    return rs.getInt("total");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(EntitieCiudad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<EntitieCiudad> list() {
        try (Connection conectar = new Conexion().conectar()) {
            List<EntitieCiudad> listCiudad = new ArrayList();
            try (PreparedStatement pstmt = conectar.prepareStatement("SELECT * FROM karviewcnt.ciudad LIMIT 1;"); ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    listCiudad.add(new EntitieCiudad(rs.getString("idCiudad"), rs.getString("ciudad")));
                }
                return listCiudad;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EntitieCiudad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<EntitieCiudad> list(int desde, int cuantos) {
        try (Connection conectar = new Conexion().conectar()) {
            List<EntitieCiudad> listCiudad = new ArrayList();
            try (PreparedStatement pstmt = conectar.prepareStatement("SELECT * FROM karviewcnt.ciudad LIMIT " + desde + "," + cuantos + ";"
            ); ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    listCiudad.add(new EntitieCiudad(rs.getString("idCiudad"), rs.getString("ciudad")));
                }
                return listCiudad;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EntitieCiudad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
