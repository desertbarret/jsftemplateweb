/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model.entitie;

/**
 *
 * @author desertbarret
 */
public class EntitieCiudad {

    private String id;
    private String ciudad;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public EntitieCiudad(String id, String ciudad) {
        this.id = id;
        this.ciudad = ciudad;
    }
}
